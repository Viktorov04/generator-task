const sum = (a, b) => {
    if(!a || !b) {
        throw new TypeError();
    }
    return a + b;
}

function* z(a, b) {
    console.log('start');

    let sumRes;

    try {
        sumRes = yield () => sum(a, b); // 6
        console.log('res 1: ' + sumRes);
    } catch {
        console.log('res 1: ==> error');
    }

    let promiseRes = yield Promise.resolve(10); // 10
    console.log('res 2: ' + promiseRes);

    sumRes = yield () => sum(promiseRes, promiseRes); // 20
    console.log('res 3: ' + sumRes);

    promiseRes = yield Promise.resolve(sumRes); // 20
    console.log('res 4: ' + promiseRes);

    res = yield 3 + 3; // 6

    console.log('finish');
    return res;
}

function worker(gen, ...args) {
    const generator = gen(...args);
    const res = generator.next();

    function handler(res) {
        if (typeof res.value === 'function') {
            try {
                let resToPass = res.value();
                const curRes = generator.next(resToPass);

                return handler(curRes);
            } catch (error) {
                return handler(generator.throw());
            }
        }

        if (res.done) {
            return res.value;
        }

        if (res.value instanceof Promise) {
            return res.value.then(res => handler(generator.next(res)));
        }

        return handler(generator.next(res.value));
    }
    return new Promise((resolve) => resolve(handler(res)));
}


worker(z, 3, 3).then(console.log);